create table ticket
(
    id     serial
        constraint ticket_pk
            primary key,
    name   varchar,
    date   date,
    amount integer,
    price  integer
);

alter table ticket
    owner to postgres;

INSERT INTO public.ticket (id, name, date, amount, price) VALUES (6, 'Виступ Дмитра Комарова', '2022-01-05', 25, 400);
INSERT INTO public.ticket (id, name, date, amount, price) VALUES (4, 'Джаз-фестиваль в парку', '2021-12-01', 20, 300);
INSERT INTO public.ticket (id, name, date, amount, price) VALUES (5, 'Різдвяна казка у Києві', '2021-12-25', 0, 400);
INSERT INTO public.ticket (id, name, date, amount, price) VALUES (2, 'Концерт групи Бумбокс ', '2021-12-25', 40, 700);
INSERT INTO public.ticket (id, name, date, amount, price) VALUES (3, 'Фільм під відкритим небом', '2021-12-08', 15, 300);
INSERT INTO public.ticket (id, name, date, amount, price) VALUES (1, 'Футбол Італія-Англія', '2021-12-24', 350, 2900);