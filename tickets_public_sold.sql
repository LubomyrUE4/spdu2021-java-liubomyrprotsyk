create table sold
(
    name           varchar,
    date           date,
    price          integer,
    buyer          varchar,
    id             serial
        constraint sold_pk
            primary key,
    "wantedAmount" integer
);

alter table sold
    owner to postgres;

INSERT INTO public.sold (name, date, price, buyer, id, "wantedAmount") VALUES ('Концерт групи Бумбокс', '2021-12-25', 700, 'Невідомий', 1, 1);
INSERT INTO public.sold (name, date, price, buyer, id, "wantedAmount") VALUES ('Виступ Дмитра Комарова', '2022-01-05', 400, 'Невідомий', 2, 1);
INSERT INTO public.sold (name, date, price, buyer, id, "wantedAmount") VALUES ('Джаз-фестиваль в парку', '2021-12-01', 600, 'Олег Величко', 3, 2);
INSERT INTO public.sold (name, date, price, buyer, id, "wantedAmount") VALUES ('Виступ Дмитра Комарова', '2022-01-05', 400, 'Невідомий', 4, 1);
INSERT INTO public.sold (name, date, price, buyer, id, "wantedAmount") VALUES ('Джаз-фестиваль в парку', '2021-12-01', 300, 'Невідомий', 5, 1);