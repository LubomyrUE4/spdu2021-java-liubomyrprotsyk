package com.example.spdu2021javaliubomyrprotsyk.dao;

import com.example.spdu2021javaliubomyrprotsyk.models.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class TicketsDAO {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TicketsDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Ticket> getAll() {
        return jdbcTemplate.query("SELECT * FROM ticket", new BeanPropertyRowMapper<>(Ticket.class));
    }

    public Ticket getById(int id) {
        return jdbcTemplate.query("SELECT * FROM ticket WHERE id=?", new BeanPropertyRowMapper<>(Ticket.class), id)
                .stream().findAny().orElse(null);
    }

    public void newEvent(Ticket ticket) {
        jdbcTemplate.update("INSERT INTO ticket VALUES((SELECT MAX(id)+1 FROM ticket), ?, ?, ?, ?)",
                ticket.getName(), ticket.getDate(), ticket.getAmount(), ticket.getPrice());
    }

    public void buy(Ticket ticket) {
        jdbcTemplate.update("UPDATE ticket SET amount=? WHERE id=?", ticket.getAmount() - ticket.getWantedAmount(), ticket.getId());
    }

    //log
    public void addToLog(Ticket soldTicket) {
        jdbcTemplate.update("INSERT INTO sold VALUES(?, ?, ?, ?, (SELECT MAX(id)+1 FROM sold), ?)",
                soldTicket.getName(), soldTicket.getDate(), soldTicket.getPrice()*soldTicket.getWantedAmount(), soldTicket.getBuyer(), soldTicket.getWantedAmount());
    }
}