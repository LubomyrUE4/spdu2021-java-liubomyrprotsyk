package com.example.spdu2021javaliubomyrprotsyk.controllers;

import com.example.spdu2021javaliubomyrprotsyk.dao.TicketsDAO;
import com.example.spdu2021javaliubomyrprotsyk.models.Ticket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Controller
public class TicketsController {
    private TicketsDAO ticketsDAO;

    Logger logger = LoggerFactory.getLogger(Ticket.class);

    public TicketsController(TicketsDAO ticketsDAO) {
        this.ticketsDAO = ticketsDAO;
    }

    @GetMapping
    private String getAll(@RequestParam(value = "sort", required = false) String sort,
            Model model) {
        model.addAttribute("tickets", ticketsDAO.getAll());
        //сортувати в html
        if (sort != null) {
            if (sort.toLowerCase().equals("bydate")) {
                model.addAttribute("comparator", Comparator.comparing(Ticket::getDate));

                //сортувати в консоль
                List<Ticket> sortedArray = new ArrayList<>();
                sortedArray = ticketsDAO.getAll();

                Collections.sort(sortedArray, new Comparator<Ticket>() {
                    public int compare(Ticket t1, Ticket t2) {
                        return t1.getDate().compareTo(t2.getDate());
                    }
                });
                AtomicInteger count = new AtomicInteger(1);
                sortedArray.stream().forEach((t) -> logger.info(count.getAndIncrement() + ". " + t.getName() + " " + t.getDate()));
            }
            else if (sort.toLowerCase().equals("byid"))
                model.addAttribute("comparator", Comparator.comparing(Ticket::getId));
        }
        else
            model.addAttribute("comparator", Comparator.comparing(Ticket::getId));

        return "all";
    }

    @GetMapping("/new")
    private String newPage(@ModelAttribute("ticket") Ticket ticket) {
        return "newEvent";
    }

    @PostMapping
    private String createNew(@ModelAttribute("ticket") Ticket ticket)
    {
        ticketsDAO.newEvent(ticket);
        return "redirect:/";
    }

    @PostMapping("/buy/{id}")
    private String buy(@PathVariable int id) {
        Ticket soldTicket = ticketsDAO.getById(id);
        ticketsDAO.buy(soldTicket);
        String logInfo = ("Куплено: " + soldTicket.getName() + " " + soldTicket.getDate() + " " + "- " +
                soldTicket.getPrice()*soldTicket.getWantedAmount() + " грн (" + soldTicket.getWantedAmount() + " шт.) ");
        logger.info(logInfo);
        ticketsDAO.addToLog(soldTicket);
        return "buy";
    }

    @GetMapping("/sell/{id}")
    private String sellPage(@PathVariable int id, Model model) {
        model.addAttribute("ticket", ticketsDAO.getById(id));
        return "sellById";
    }

    @PostMapping("/sell/{id}")
    private String sell(@PathVariable int id,
                        @RequestParam(value = "wantedAmount") int wantedAmount,
                        @RequestParam(value = "buyer") String buyer,
                        Model model) {
        Ticket ticket = ticketsDAO.getById(id);
        ticket.setWantedAmount(wantedAmount); ticket.setBuyer(buyer);

        if (ticket.getAmount() > 0 && ticket.getWantedAmount()<=ticket.getAmount()) {
            ticketsDAO.buy(ticket);

            logger.info("Продано: " + ticket.getName() + " " + ticket.getDate() + " " + "- " +
                    ticket.getPrice()*ticket.getWantedAmount() + " грн (" + ticket.getWantedAmount() + " шт.) " +
                    "Покупець: " + ticket.getBuyer());
            ticketsDAO.addToLog(ticket);
        } else
            logger.info("Такої кількості квитків немає");

        return "redirect:/";
    }

}