package com.example.spdu2021javaliubomyrprotsyk.models;


import java.sql.Date;

public class Ticket {
    private int id;
    private String name;
    private Date date;
    private int price;
    private int amount;

    private int wantedAmount = 1;
    private String buyer = "Невідомий";

    public int getWantedAmount() {
        return wantedAmount;
    }

    public void setWantedAmount(int wantedAmount) {
        this.wantedAmount = wantedAmount;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Ticket() {
    }

    public Ticket(int id, String name, Date date, int price, int amount) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.price = price;
        this.amount = amount;
        this.buyer = buyer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
