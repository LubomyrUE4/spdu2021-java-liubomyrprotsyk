package com.example.spdu2021javaliubomyrprotsyk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spdu2021JavaLiubomyrprotsykApplication {

    public static void main(String[] args) {
        SpringApplication.run(Spdu2021JavaLiubomyrprotsykApplication.class, args);
    }

}
